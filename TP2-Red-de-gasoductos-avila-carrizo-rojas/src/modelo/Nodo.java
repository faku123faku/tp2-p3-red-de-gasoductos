package modelo;

//Nodo que contiene las caracteristicas necesarias para la red de gasoducto.
public class Nodo {

	private int _numero;
	private String _tipo; //Especificar si es productor, consumidor, de paso
	private int _capacidadMax;
	private Nodo _nodoSiguiente; //Indica cual es el proximo nodo al que se dirige
	private Nodo _nodoAnterior; //Indica cual fue el nodo anterior
	
	Nodo (int num, String tipo, int cap){
		_numero=num;
		_tipo=tipo;
		_capacidadMax=cap;
	}
	
	public int getNum() {
		return _numero;
	}
	public String getTipo() {
		return _tipo;
	}
	
	public int getCapMax() {
		return _capacidadMax;
	}
}
